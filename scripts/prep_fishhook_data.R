


# The objective of this script is to generate the data objects we'll use for the fish.hook model:
# - gr:         our merged TCGA CNA data with a field, 'seg.mean' that is used to define amp/del 
#               'events'; 
# - targets:    all genes in human genome
# - eligible:   the subset of our data that has adequate coverage/data quality (number of reads)
# - gr.tiles:   the length of the human genome (all chromosomes) split into 'tiles' of 100Kb size
#               (this is to be added as a covariate in the fish.hook model, to control for local 
#               mutational density variation)
#
# Because the objective of our benchmarking analysis is to compare fish.hook's output to that 
# from GISTIC2, we will filter our merged TCGA data include only the samples and genes that 
# were in the data for GISTIC2. This may not be necessary, but my assumption is that it makes 
# comparing the two methods more rigorous.  


# set the working directory
work.dir <- '~/NYGC/Projects/fishhook_benchmark'
setwd(work.dir)

# load the required packages
suppressMessages(library(data.table))
suppressMessages(library(GenomicRanges))
suppressMessages(library(gTrack))
suppressMessages(library(skitools))
suppressMessages(library(skidb))
suppressMessages(library(ffTrack))

# source the fishhook functions rather than load the package from github to
# get around a workspace-bug in the package (ask marcin)
source('~/NYGC/git/dev/fish.hook/R/fishhook.R')


# load the merged TCGA BRCA data (see get_tcga_cna_data.R)
cna_data <- 'data/tcga_brca_pub_merged.rds'
d.original=readRDS(cna_data)

# Note:
# Our intention is to compare the output from the fishhook model against that run by TCGA using 
# their GISTIC2 software. A problem is that the data in the GISTIC2 directory appears to 
# contain fewer samples than our merged data. (My assumption is that they ran the data through 
# a QC pipeline and ended up retaining only these samples.) 
#
# I attempted to subset our merged data to only include the samples that were used in the 
# GISTIC data, but the Sample IDs in the two datasets don't match at all. This is because TCGA 
# uses various formats for their samples' identification barcodes (google: TCGA Sample Barcodes)
#
# One file in the GISTIC2/ directory had a translation between these IDs, so I'm using it to 
# convert the merged-data's sample IDs to the format in GISTIC2/, and then filter the merged 
# data down to only the samples in the GISTIC/ analysis. The end result is not perfect; for 
# some reason there are some samples in the GISTIC2/ data that were not in our merged data file. 
# I just proceeded with the subset of data that DID match, but this problem may need to be 
# resolved. -AG

# translate the sample IDs in the merged data to the format for the IDs in the GISTIC2 analysis
# [e.g. convert 'BEAUX_p_TCGA_b109_SNP_2N_GenomeWideSNP_6_A01_772082' to 'TCGA-A1-A0SB-01']
gistic_sample_summary <- fread('data/tcga_brca_pub/broad.mit.edu_BRCA.Genome_Wide_SNP_6.mage-tab.1.1008.0/broad.mit.edu_BRCA.Genome_Wide_SNP_6.sdrf.txt')
gistic_sample_summary <- gistic_sample_summary[,c('Extract Name','Hybridization Name'),with=F]
gistic_sample_summary$`Extract Name` <- paste0(strtrim(gistic_sample_summary$`Extract Name`,13),'01')
d <- merge(d.original,gistic_sample_summary,by.x='ID',by.y='Hybridization Name',all.x=T)
d$ID = d$`Extract Name`; d$`Extract Name`= NULL


# filter 'gr' to only include the samples they used
gistic.ids <- read.csv('data/GISTIC2/array_list.txt')[[1]]
gr <- seg2gr(d[d$ID %in% gistic.ids,])


# Note the problem: 
length(unique(d.original$ID)) # number of samples in our merged data
length(unique(gistic_sample_summary$`Extract Name`)) # number of samples used by TCGA in GISTIC2

# number of samples in merged data that were in the GISTIC2 data (should == 813)
length(unique(gr$ID)) 


# define the targets for fish.hook;
# filter targets to only include the genes used in Gistic analysis
targets <- read_gencode('gene')
all_data_by_genes <- fread('data/GISTIC2/all_data_by_genes.txt')
gistic_genes <- sort(unique(all_data_by_genes$`Gene Symbol`))
targets <- targets[targets$gene_name %in% gistic_genes,]


# tile the genome (of 100Kb length) and create a track for the mean-seg.mean within each tile
# this will be a covariate in the fish.hook model, to control for local mutational density
sl <- seqlengths(targets)
tiles <- gr.tile(sl,1e5)
gr.tiles <- gr.val(tiles, gr, val='seg.mean', max.chunk = Inf, mc.cores = 10, max.slice = 1e5)
gr.tiles <- gr.tiles[!is.na(gr.tiles$seg.mean),]


# define the eligible regions (based on their coverage exceeding 1500 reads)
eligible <- as(coverage(gr), 'GRanges') %Q% (score>1500)


# save the prepared GRanges data required for the fish.hook model
save(gr,eligible,targets,gr.tiles,file='data/tcga_brca_prepped.RData')



