

# set the working directory
work.dir <- '~/NYGC/Projects/fishhook_benchmark'
setwd(work.dir)


# 0. Download/extract the BRCA CNA data (level 3) from TCGA's website: 
# https://tcga-data.nci.nih.gov/docs/publications/brca_2012/
url <- 'http://tcga-data.nci.nih.gov/docs/publications/brca_2012/BRCA.Copy_Number.tar'
system(paste('wget -O data/BRCA.Copy_Number.tar',url))
system('tar xvf data/BRCA.Copy_Number.tar --directory data/')


# 1. unzip all the data files from BRCA_Copy_Number_Archives to a single directory:
# data/tcga_brca_pub (pub = public)
extracted = 'data/BRCA_Copy_Number_Archives'
gzfiles <- file.path(extracted,grep('.gz',dir(extracted),value=T))
dir.create('data/tcga_brca_pub')
f <- function(f) {system(paste0('tar xfvz ',f,' --directory data/tcga_brca_pub'))}
lapply(gzfiles,f)


# 2. extract the sample data from each unziped file into a new directory 'all_samples'
# these files will be loaded and merged into a single data.table
tcga.pub <- 'data/tcga_brca_pub'
datafiles <- file.path(tcga.pub,grep('broad.mit.edu',dir(tcga.pub),value=T))
samples.dir <- 'data/tcga_brca_pub/samples'
f <- function(f) {system(paste0('cp ',f,'/* ',samples.dir))}
lapply(datafiles,f)


# 3. load each sample's data and merge them into data.table; save it to the data/ directory
mergedfile <- 'data/tcga_brca_pub_merged.rds'
files = file.path(samples.dir,dir(samples.dir))
files = grep('seg.data.txt',files,value=T)
loadData <- function(file) { fread(file) }
datalist = lapply(files,loadData)
d = rbindlist(datalist)
saveRDS(d,mergedfile)
message('done')



